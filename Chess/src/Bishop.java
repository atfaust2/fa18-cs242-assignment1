import java.util.HashSet;

public class Bishop extends Piece {

    public Bishop(boolean player, int position) {
        super(player, position);
        this.name = "Bishop";
    }

    /*
     * An implementation of the Bishops behavior
     */
    @Override
    HashSet<Integer> computeValidMoveSet() {
        HashSet<Integer> moveSet = new HashSet<Integer>();
        moveSet.add(this.position - 19);
        moveSet.add(this.position - 38);
        moveSet.add(this.position - 57);
        moveSet.add(this.position - 76);
        moveSet.add(this.position - 95);
        moveSet.add(this.position - 114);
        moveSet.add(this.position - 133);
        moveSet.add(this.position + 19);
        moveSet.add(this.position + 38);
        moveSet.add(this.position + 57);
        moveSet.add(this.position + 76);
        moveSet.add(this.position + 95);
        moveSet.add(this.position + 114);
        moveSet.add(this.position + 133);

        moveSet.add(this.position - 21);
        moveSet.add(this.position - 42);
        moveSet.add(this.position - 63);
        moveSet.add(this.position - 84);
        moveSet.add(this.position - 105);
        moveSet.add(this.position - 126);
        moveSet.add(this.position - 147);
        moveSet.add(this.position + 21);
        moveSet.add(this.position + 42);
        moveSet.add(this.position + 63);
        moveSet.add(this.position + 84);
        moveSet.add(this.position + 105);
        moveSet.add(this.position + 126);
        moveSet.add(this.position + 147);
        return moveSet;
    }
}
