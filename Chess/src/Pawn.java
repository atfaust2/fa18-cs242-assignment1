import java.util.HashSet;

public class Pawn extends Piece {
    boolean hasMoved;

    public Pawn(boolean player, int position) {
        super(player, position);
        hasMoved = false;
        this.name = "Pawn";
    }

    /*
     * Pawns can move either one or two squares forward depending on if they have been
     * moved yet or not which is implemented below
     */
    @Override
    HashSet<Integer> computeValidMoveSet() {
        HashSet<Integer> moveSet = new HashSet<Integer>();
        if(hasMoved) {
            if(player) {
                moveSet.add(this.position + 20);
            }
            else {
                moveSet.add(this.position - 20);
            }
        }
        else {
            if(player) {
                moveSet.add(this.position + 20);
                moveSet.add(this.position + 40);
            }
            else {
                moveSet.add(this.position - 20);
                moveSet.add(this.position - 40);
            }
        }
        return moveSet;
    }

    /*
     * We override move in the pawn so that we can indicated if the pawn has moved
     * yet or not
     */
    @Override
    int move(int position) {
        int rtn = super.move(position);
        this.hasMoved = true;
        return rtn;
    }
}
