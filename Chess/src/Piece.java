import java.util.HashSet;

/*
 * This is an abstract class to represent a generic chess piece
 * The behavior of the piece is implemented inside of "computeValidMoveSet"
 */
public abstract class Piece {
    boolean player;
    int position;
    String name = "";
    private HashSet<Integer> cachedMoveSet;

    /*
     * The constructor for a piece
     */
    public Piece(boolean player, int position) {
        this.player = player;
        this.position = position;
        this.cachedMoveSet = new HashSet<Integer>();
    }

    /*
     * A function to retrieve the set of all valid moves for a piece. Serves to prune
     * out squares that are not on the board.
     */
    HashSet<Integer> getValidMoveSet() {
        if(this.cachedMoveSet.isEmpty()) {
            this.cachedMoveSet = computeValidMoveSet();
        }
        this.cachedMoveSet.removeIf(i -> !positionIsValid(i));
        return this.cachedMoveSet;
    }

    /*
     * Every piece must tell me how it is allowed to move. This may be implemented such that
     * it may return items that are not valid squares on the board.
     */
    abstract HashSet<Integer> computeValidMoveSet();

    /*
     * This is a function that moves the piece and returns the position we moved to
     * if the move is valid, or returns -1 if the move is not valid and does nothing
     */
    int move(int position) {
        if(getValidMoveSet().contains(position)) {
            this.cachedMoveSet.clear();
            this.position = position;
            return this.position;
        }
        else {
            return -1;
        }
    }

    /*
     * This checks if a position is on the board given that the board is a typical
     * chess board and we are using the encoding scheme for the board that we defined
     * in the Game comments
     */
    private boolean positionIsValid(int position) {
        boolean isValid = false;
        isValid = isValid || (position > -1) && (position < 8);
        isValid = isValid || (position > 19) && (position < 28);
        isValid = isValid || (position > 39) && (position < 48);
        isValid = isValid || (position > 59) && (position < 68);
        isValid = isValid || (position > 79) && (position < 88);
        isValid = isValid || (position > 99) && (position < 108);
        isValid = isValid || (position > 119) && (position < 128);
        isValid = isValid || (position > 139) && (position < 148);
        return isValid;

    }

}
