import java.util.HashSet;

public class Knight extends Piece {

    public Knight(boolean player, int position) {
        super(player, position);
        this.name = "Knight";
    }

    /*
     * an implementation of the Knights move behavior
     */
    @Override
    HashSet<Integer> computeValidMoveSet() {
        HashSet<Integer> moveSet = new HashSet<Integer>();
        moveSet.add(this.position + 22);
        moveSet.add(this.position + 18);
        moveSet.add(this.position + 41);
        moveSet.add(this.position + 39);
        moveSet.add(this.position - 22);
        moveSet.add(this.position - 18);
        moveSet.add(this.position - 41);
        moveSet.add(this.position - 39);
        return moveSet;
    }
}
