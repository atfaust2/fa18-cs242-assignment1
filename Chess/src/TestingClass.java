import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestingClass {


    @Test
    void testDefaultGameSetup() {
        Game game = new Game();
        assertEquals(game.pieces.size(), 32);

        assert(game.getValidMoveSet(0).isEmpty());
        assert(game.getValidMoveSet(2).isEmpty());
        assert(game.getValidMoveSet(3).isEmpty());
        assert(game.getValidMoveSet(4).isEmpty());
        assert(game.getValidMoveSet(5).isEmpty());
        assert(game.getValidMoveSet(7).isEmpty());

        System.out.println(game.getValidMoveSet(140));
        assert(game.getValidMoveSet(140).isEmpty());
        assert(game.getValidMoveSet(142).isEmpty());
        assert(game.getValidMoveSet(143).isEmpty());
        assert(game.getValidMoveSet(144).isEmpty());
        assert(game.getValidMoveSet(145).isEmpty());
        assert(game.getValidMoveSet(147).isEmpty());
    }
    //Test Pawn First Move
    @Test
    void pawnFirstMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Pawn(false, 120));
        pieces.add(new Pawn(true, 20));

        Game game = new Game(pieces);
        assertEquals(game.getValidMoveSet(20).size(), 2);
        assert(game.getValidMoveSet(20).contains(40));
        assert(game.getValidMoveSet(20).contains(60));

        assertEquals(game.getValidMoveSet(120).size(), 2);
        assert(game.getValidMoveSet(120).contains(100));
        assert(game.getValidMoveSet(120).contains(80));

        assertNotEquals(-1, game.move(20, 60));
        assertNotEquals(-1, game.move(120, 80));

    }

    //Test Pawn Second Move

    @Test
    void pawnAfterFirstMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        //Black Pawns
        pieces.add(new Pawn(false, 120));
        //White Pawns
        pieces.add(new Pawn(true, 21));

        Game game = new Game(pieces);

        assertNotEquals(-1, game.move(21, 61));
        assertNotEquals(-1, game.move(120, 80));

        assertEquals(-1, game.move(61, 101));
        assertEquals(-1, game.move(80, 40));

        assert(game.getValidMoveSet(61).contains(81));
        assert(game.getValidMoveSet(80).contains(60));
    }

    //Test pawn if it has another piece in front of it
    @Test
    void pawnObstructed() {
        ArrayList<Piece> pieces = new ArrayList<>();
        //Black Pawns
        pieces.add(new Pawn(false, 120));
        //White Pawns
        pieces.add(new Pawn(true, 20));

        Game game = new Game(pieces);

        assertNotEquals(-1, game.move(20, 60));
        assertNotEquals(-1, game.move(120, 80));

        assertEquals(-1, game.move(60, 100));
        assertEquals(-1, game.move(80, 40));

        assert(game.getValidMoveSet(60).isEmpty());
        System.out.println(game.getValidMoveSet((80)));
        assert(game.getValidMoveSet(80).isEmpty());

    }

    //Test Pawn Capture
    @Test
    void pawnCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        //Black Pawns
        pieces.add(new Pawn(false, 60));
        //White Pawns
        pieces.add(new Pawn(true, 41));

        Game game = new Game(pieces);

        assert(game.getValidMoveSet(60).contains(41));
        assert(game.getValidMoveSet(41).contains(60));
        assertEquals(game.move(41, 60), 60);
    }

    //Test Rook Move

    @Test
    void rookMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Rook(false, 0));
        pieces.add(new Rook(true, 147));

        Game game = new Game(pieces);

        assertEquals(game.getValidMoveSet(0).size(), 14);
        assert(game.getValidMoveSet(0).contains(20));
        assert(game.getValidMoveSet(0).contains(40));
        assert(game.getValidMoveSet(0).contains(60));
        assert(game.getValidMoveSet(0).contains(80));
        assert(game.getValidMoveSet(0).contains(100));
        assert(game.getValidMoveSet(0).contains(120));
        assert(game.getValidMoveSet(0).contains(140));

        assert(game.getValidMoveSet(0).contains(1));
        assert(game.getValidMoveSet(0).contains(2));
        assert(game.getValidMoveSet(0).contains(3));
        assert(game.getValidMoveSet(0).contains(4));
        assert(game.getValidMoveSet(0).contains(5));
        assert(game.getValidMoveSet(0).contains(6));
        assert(game.getValidMoveSet(0).contains(7));

        assertEquals(game.getValidMoveSet(147).size(), 14);
        assert(game.getValidMoveSet(147).contains(7));
        assert(game.getValidMoveSet(147).contains(27));
        assert(game.getValidMoveSet(147).contains(47));
        assert(game.getValidMoveSet(147).contains(67));
        assert(game.getValidMoveSet(147).contains(87));
        assert(game.getValidMoveSet(147).contains(107));
        assert(game.getValidMoveSet(147).contains(127));

        assert(game.getValidMoveSet(147).contains(140));
        assert(game.getValidMoveSet(147).contains(141));
        assert(game.getValidMoveSet(147).contains(142));
        assert(game.getValidMoveSet(147).contains(143));
        assert(game.getValidMoveSet(147).contains(144));
        assert(game.getValidMoveSet(147).contains(145));
        assert(game.getValidMoveSet(147).contains(146));
    }


    //Test Rook Capture

    @Test
    void rookCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Rook(false, 0));
        pieces.add(new Rook(true, 140));


        Game game = new Game(pieces);
        assertEquals(game.pieces.size(), 2);
        assertEquals(game.move(140, 0), 0);
        assertEquals(game.pieces.size(), 1);
    }

    //Test Bishop Move

    @Test
    void bishopMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Bishop(false, 63));
        pieces.add(new Bishop(true, 64));

        Game game = new Game(pieces);

        assertEquals(game.getValidMoveSet(63).size(), 13);

        assert(game.getValidMoveSet(63).contains(44));
        assert(game.getValidMoveSet(63).contains(25));
        assert(game.getValidMoveSet(63).contains(6));
        assert(game.getValidMoveSet(63).contains(82));
        assert(game.getValidMoveSet(63).contains(101));
        assert(game.getValidMoveSet(63).contains(120));

        assert(game.getValidMoveSet(63).contains(84));
        assert(game.getValidMoveSet(63).contains(105));
        assert(game.getValidMoveSet(63).contains(126));
        assert(game.getValidMoveSet(63).contains(147));
        assert(game.getValidMoveSet(63).contains(42));
        assert(game.getValidMoveSet(63).contains(21));
        assert(game.getValidMoveSet(63).contains(0));

        assertEquals(game.getValidMoveSet(64).size(), 13);

        assert(game.getValidMoveSet(64).contains(45));
        assert(game.getValidMoveSet(64).contains(26));
        assert(game.getValidMoveSet(64).contains(7));
        assert(game.getValidMoveSet(64).contains(83));
        assert(game.getValidMoveSet(64).contains(102));
        assert(game.getValidMoveSet(64).contains(121));

        assert(game.getValidMoveSet(64).contains(85));
        assert(game.getValidMoveSet(64).contains(106));
        assert(game.getValidMoveSet(64).contains(127));
        assert(game.getValidMoveSet(64).contains(43));
        assert(game.getValidMoveSet(64).contains(22));
        assert(game.getValidMoveSet(64).contains(1));
    }
    //Test Bishop Capture

    @Test
    void bishopCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Bishop(false, 0));
        pieces.add(new Bishop(true, 147));


        Game game = new Game(pieces);
        assertEquals(game.pieces.size(), 2);
        assertEquals(game.move(147, 0), 0);
        assertEquals(game.pieces.size(), 1);
    }

    //Test Knight Move
    @Test
    void knightMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Knight(false, 63));

        Game game = new Game(pieces);
        assertEquals(game.getValidMoveSet(63).size(), 8);

        assert(game.getValidMoveSet(63).contains(85));
        assert(game.getValidMoveSet(63).contains(41));
        assert(game.getValidMoveSet(63).contains(104));
        assert(game.getValidMoveSet(63).contains(22));
        assert(game.getValidMoveSet(63).contains(102));
        assert(game.getValidMoveSet(63).contains(24));
        assert(game.getValidMoveSet(63).contains(81));
        assert(game.getValidMoveSet(63).contains(45));

    }
    //Test Knight Capture
    @Test
    void knightCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Knight(false, 0));
        pieces.add(new Knight(true, 22));


        Game game = new Game(pieces);
        assertEquals(game.pieces.size(), 2);
        assertEquals(game.move(22, 0), 0);
        assertEquals(game.pieces.size(), 1);
    }

    //Test King Move
    @Test
    void kingMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new King(false, 63));

        Game game = new Game(pieces);
        assertEquals(game.getValidMoveSet(63).size(), 8);

        assert(game.getValidMoveSet(63).contains(42));
        assert(game.getValidMoveSet(63).contains(43));
        assert(game.getValidMoveSet(63).contains(44));
        assert(game.getValidMoveSet(63).contains(62));
        assert(game.getValidMoveSet(63).contains(64));
        assert(game.getValidMoveSet(63).contains(82));
        assert(game.getValidMoveSet(63).contains(83));
        assert(game.getValidMoveSet(63).contains(84));

    }

    //Test King Capture
    @Test
    void kingCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new King(true, 1));
        pieces.add(new Knight(false, 0));


        Game game = new Game(pieces);
        assertEquals(game.pieces.size(), 2);
        assertEquals(game.move(1, 0), 0);
        assertEquals(game.pieces.size(), 1);
    }

    //Test Queen Move
    @Test
    void queenMove() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Queen(false, 63));

        Game game = new Game(pieces);

        assertEquals(game.getValidMoveSet(63).size(), 27);

        assert(game.getValidMoveSet(63).contains(44));
        assert(game.getValidMoveSet(63).contains(25));
        assert(game.getValidMoveSet(63).contains(6));
        assert(game.getValidMoveSet(63).contains(82));
        assert(game.getValidMoveSet(63).contains(101));
        assert(game.getValidMoveSet(63).contains(120));

        assert(game.getValidMoveSet(63).contains(84));
        assert(game.getValidMoveSet(63).contains(105));
        assert(game.getValidMoveSet(63).contains(126));
        assert(game.getValidMoveSet(63).contains(147));
        assert(game.getValidMoveSet(63).contains(42));
        assert(game.getValidMoveSet(63).contains(21));
        assert(game.getValidMoveSet(63).contains(0));

        assert(game.getValidMoveSet(63).contains(43));
        assert(game.getValidMoveSet(63).contains(23));
        assert(game.getValidMoveSet(63).contains(3));
        assert(game.getValidMoveSet(63).contains(83));
        assert(game.getValidMoveSet(63).contains(103));
        assert(game.getValidMoveSet(63).contains(123));
        assert(game.getValidMoveSet(63).contains(143));

        assert(game.getValidMoveSet(63).contains(60));
        assert(game.getValidMoveSet(63).contains(61));
        assert(game.getValidMoveSet(63).contains(62));
        assert(game.getValidMoveSet(63).contains(64));
        assert(game.getValidMoveSet(63).contains(65));
        assert(game.getValidMoveSet(63).contains(66));
        assert(game.getValidMoveSet(63).contains(67 ));

    }

    //Test Queen Capture
    @Test
    void queenCapture() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Queen(true, 1));
        pieces.add(new Pawn(false, 0));


        Game game = new Game(pieces);
        assertEquals(game.pieces.size(), 2);
        assertEquals(game.move(1, 0), 0);
        assertEquals(game.pieces.size(), 1);
    }
    //Test Castle


    //Test Check
    @Test
    void check() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new King(true, 0));
        pieces.add(new Knight(false, 22));
        Game game = new Game(pieces);
        assert(game.isCheck());
    }

    //Test checkmate
    @Test
    void checkmate() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new King(true, 0));
        pieces.add(new Rook(false, 141));
        pieces.add(new Rook(false, 27));
        pieces.add(new Bishop(false, 147));
        Game game = new Game(pieces);
        assert(game.isCheckMate());
    }

    //Test stalemate
    @Test
    void stalemate() {
        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Pawn(true, 40));
        pieces.add(new Pawn(false, 60));
        Game game = new Game(pieces);
        assert(game.isStalemate());
    }
}