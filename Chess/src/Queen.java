import java.util.HashSet;

public class Queen extends Piece {

    public Queen(boolean player, int position) {
        super(player, position);
        this.name = "Queen";
    }

    /*
     * Just an implementation of the queens behavior
     */
    @Override
    HashSet<Integer> computeValidMoveSet() {
        HashSet<Integer> moveSet = new HashSet<Integer>();
        moveSet.add(this.position - 1);
        moveSet.add(this.position - 2);
        moveSet.add(this.position - 3);
        moveSet.add(this.position - 4);
        moveSet.add(this.position - 5);
        moveSet.add(this.position - 6);
        moveSet.add(this.position - 7);
        moveSet.add(this.position + 1);
        moveSet.add(this.position + 2);
        moveSet.add(this.position + 3);
        moveSet.add(this.position + 4);
        moveSet.add(this.position + 5);
        moveSet.add(this.position + 6);
        moveSet.add(this.position + 7);

        moveSet.add(this.position - 20);
        moveSet.add(this.position - 40);
        moveSet.add(this.position - 60);
        moveSet.add(this.position - 80);
        moveSet.add(this.position - 100);
        moveSet.add(this.position - 120);
        moveSet.add(this.position - 140);
        moveSet.add(this.position + 20);
        moveSet.add(this.position + 40);
        moveSet.add(this.position + 60);
        moveSet.add(this.position + 80);
        moveSet.add(this.position + 100);
        moveSet.add(this.position + 120);
        moveSet.add(this.position + 140);

        moveSet.add(this.position - 19);
        moveSet.add(this.position - 38);
        moveSet.add(this.position - 57);
        moveSet.add(this.position - 76);
        moveSet.add(this.position - 95);
        moveSet.add(this.position - 114);
        moveSet.add(this.position - 133);
        moveSet.add(this.position + 19);
        moveSet.add(this.position + 38);
        moveSet.add(this.position + 57);
        moveSet.add(this.position + 76);
        moveSet.add(this.position + 95);
        moveSet.add(this.position + 114);
        moveSet.add(this.position + 133);

        moveSet.add(this.position - 21);
        moveSet.add(this.position - 42);
        moveSet.add(this.position - 63);
        moveSet.add(this.position - 84);
        moveSet.add(this.position - 105);
        moveSet.add(this.position - 126);
        moveSet.add(this.position - 147);
        moveSet.add(this.position + 21);
        moveSet.add(this.position + 42);
        moveSet.add(this.position + 63);
        moveSet.add(this.position + 84);
        moveSet.add(this.position + 105);
        moveSet.add(this.position + 126);
        moveSet.add(this.position + 147);

        return moveSet;
    }
}
