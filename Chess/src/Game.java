import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/*
 * This class represents a game as an array of all the pieces and a boolean indicating
 * whose turn it currently is.
 * This class handles behavior that only takes place as a result of 2 or more
 * pieces interacting such as capture.
 *
 * Currently the board is represented as such:
 *      The first row is 0 - 7, the second is 20 - 27, the third is 40 - 47 ...
 */

class Game {

    ArrayList<Piece> pieces;        /* The pieces of both players */
    private boolean turn;           /*  White: true, Black: false  */

    /*
     * The default constructor for the game initializes all of the pieces as per
     * the normal starting chess rules
     */
    Game() {
        pieces = new ArrayList<>();

        //White Pieces
        pieces.add(new Rook(true, 0));
        pieces.add(new Knight(true, 1));
        pieces.add(new Bishop(true, 2));
        pieces.add(new King(true, 3));
        pieces.add(new Queen(true, 4));
        pieces.add(new Bishop(true, 5));
        pieces.add(new Knight(true, 6));
        pieces.add(new Rook(true, 7));
        pieces.add(new Pawn(true, 20));
        pieces.add(new Pawn(true, 21));
        pieces.add(new Pawn(true, 22));
        pieces.add(new Pawn(true, 23));
        pieces.add(new Pawn(true, 24));
        pieces.add(new Pawn(true, 25));
        pieces.add(new Pawn(true, 26));
        pieces.add(new Pawn(true, 27));

        //Black Pieces
        pieces.add(new Pawn(false, 120));
        pieces.add(new Pawn(false, 121));
        pieces.add(new Pawn(false, 122));
        pieces.add(new Pawn(false, 123));
        pieces.add(new Pawn(false, 124));
        pieces.add(new Pawn(false, 125));
        pieces.add(new Pawn(false, 126));
        pieces.add(new Pawn(false, 127));
        pieces.add(new Rook(false, 140));
        pieces.add(new Knight(false, 141));
        pieces.add(new Bishop(false, 142));
        pieces.add(new Queen(false, 143));
        pieces.add(new King(false, 144));
        pieces.add(new Bishop(false, 145));
        pieces.add(new Knight(false, 146));
        pieces.add(new Rook(false, 147));

        //Set the turn to white
        turn = true;
    }

    /*
     * A constructor for testing purposes
     */
    Game(ArrayList<Piece> pieces) {
        this.pieces = pieces;
        turn = true;
    }

    /*
     *
     * Returns the king of the player indicated by the player parameter
     */
    private Piece getKing(boolean player) {
        return pieces.stream().filter(p -> p.name == "King" && p.player == player)
                .collect(Collectors.toList()).get(0);
    }

    /*
     * Get the piece at the position indicated by the position parameter
     */
    private Piece getPieceAtPosition(int position) {
        List<Piece> pieceAtPos =  pieces.stream().filter(p -> p.position == position)
                .collect(Collectors.toList());
        if(pieceAtPos.isEmpty())
            return null;
        return pieces.stream().filter(p -> p.position == position)
                .collect(Collectors.toList()).get(0);
    }

    /*
     *  Returns true if the player whose turn it is is in check.
     *  We are in check if our kings position intersect all the enemy valid moves is not empty
     */
    boolean isCheck() {
        HashSet<Integer> enemyValidMoves = getAllValidMoves(!turn);
        return enemyValidMoves.contains(getKing(turn).position);
    }

    /*
     * Returns true if the player whose turn it is is in checkmate.
     * We are in checkmate if we are in check and the king has no valid moves
     */
    boolean isCheckMate() {
        HashSet<Integer> kingMoves = getValidMoveSet(getKing(turn).position);
        HashSet<Integer> enemyMoves = getAllValidMoves(!turn);
        kingMoves.removeAll(enemyMoves);
        return isCheck() && kingMoves.isEmpty();
    }

    /*
     * Returns true if the player whose turn it is is in stalemate.
     * We are in stalemate if the union of all of the valid moves of all of our pieces is empty
     */
    boolean isStalemate() {
        return getAllValidMoves(turn).isEmpty();
    }

    /*
     * Returns the set of all squares that are valid moves for some piece of the player indicated
     * by the player parameter
     */
    private HashSet<Integer> getAllValidMoves(boolean player) {
        List<Piece> currentPlayersPieces = pieces.stream().filter(p -> p.player == player)
                .collect(Collectors.toList());
        HashSet<Integer> union = new HashSet<>();
        for(Piece p : currentPlayersPieces) {
            union.addAll(getValidMoveSet(p.position));
        }
        return union;
    }

    /*
     * Get the set of all of the squares that are currently occupied by a piece owned by the player
     * indicated by the player parameter
     */
    private HashSet<Integer> getAllPositions(boolean player) {
        return (HashSet<Integer>) pieces.stream().filter(p -> p.player == player)
                .map(p -> p.position).collect(Collectors.toSet());
    }

    /*
     * Move a piece in position source to another position, or return -1 if it is not possible
     */
    int move(int source, int dest) {
        List<Piece> pieceToMove = pieces.stream().filter(p -> p.position == source)
                .collect(Collectors.toList());
        if(pieceToMove.isEmpty()) {
            return -1;
        }
        assert(pieceToMove.size() == 1);
        int rtn = pieceToMove.get(0).move(dest);
        //Check if a piece was captured
        HashSet<Piece> potentialCaptures =  (HashSet<Piece>) pieces.stream().filter(p -> p.position == rtn && p.player == !turn).
                collect(Collectors.toSet());
        if(!potentialCaptures.isEmpty()) {
            pieces.removeAll(potentialCaptures);
        }
        if(rtn != -1) {
            turn = !turn;
        }
        return rtn;
    }

    /*
     * Get the valid move set for the piece at position position
     * This makes calculations that a single piece cannot make on it's own
     * like not moving into other pieces and pawn capture
     */
    HashSet<Integer> getValidMoveSet(int position) {
        List<Piece> pieceToMove = pieces.stream().filter(p -> p.position == position)
                .collect(Collectors.toList());
        if(pieceToMove.isEmpty()) {
            return null;
        }
        assert(pieceToMove.size() == 1);
        Piece p = pieceToMove.get(0);
        //Get the valid moves for that piece
        HashSet<Integer> validMoves = p.getValidMoveSet();
        //Take away all of the squares which already hold friendly pieces
        validMoves.removeAll(getAllPositions(p.player));

        /* If you are a pawn we should add the two front diagonals as moves if there is an enemy
         * there. Also we should take away front moves even if it's an enemy
         */
        if(getPieceAtPosition(position).name == "Pawn")
        {
            int pawnDirection = getPieceAtPosition(position).player ? 1 : -1;
            if(getAllPositions(!getPieceAtPosition(position).player).contains(position + (pawnDirection * 19))) {
                validMoves.add(position + (pawnDirection *19));
            }
            if(getAllPositions(!turn).contains(position + (pawnDirection * 21))) {
                validMoves.add(position + (pawnDirection * 21));
            }
            if(getPieceAtPosition(position + (pawnDirection * 20)) != null) {
                validMoves.remove(position + (pawnDirection * 20));
            }
        }

        //Take away the moves that are obstructed by any pieces
        return (HashSet<Integer>) validMoves.stream()
                .filter(candidatePos -> !isPieceBetween(position, candidatePos))
                .collect(Collectors.toSet());
    }

    /*
     * returns true iff there is a piece between source and dest not including pieces
     * at source and at dest
     */
    private boolean isPieceBetween(int source, int dest) {
        boolean inSameRow = dest - source < 8 && dest - source > -8;
        boolean inSameColumn = (dest - source) % 20  == 0;
        boolean inSameDiagonal = (dest - source) % 21 == 0 || (dest - source) % 19 == 0;
        if(!inSameRow && !inSameColumn && !inSameDiagonal) {
            //TODO: Maybe we should return some type of error in this case?
            //Assuming that the function is called with correct parameters this is a knight move.
            return false;
        }
        List<Piece> piecesBetween;
        if(inSameRow)
        {
            piecesBetween = pieces.stream().filter(p -> (p.position > source && p.position < dest)
                    || (p.position < source && p.position > dest)).collect(Collectors.toList());
            return !piecesBetween.isEmpty();
        }
        else if(inSameColumn) {
            piecesBetween = pieces.stream().filter(p -> (p.position > source && p.position < dest
                    && p.position % 20 == source % 20) || (p.position < source && p.position > dest
                    && p.position % 20 == source % 20)).collect(Collectors.toList());
        }
        else if(inSameDiagonal) {
            piecesBetween = pieces.stream().filter(p -> (p.position > source && p.position < dest
                    && ((p.position - source) % 21 == 0 || (p.position - source) % 19 == 0) ||
                    (p.position < source && p.position > dest && ((p.position - source) % 21 == 0
                            || (p.position - source) % 19 == 0)))).collect(Collectors.toList());
        }
        else {
            piecesBetween = null;
        }
        return !piecesBetween.isEmpty();
    }
    
}
