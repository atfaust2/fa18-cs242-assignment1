import java.util.HashSet;

public class King extends Piece {

    public King(boolean player, int position) {
        super(player, position);
        this.name = "King";
    }

    /*
     * The King can move one square in any direction (conditions like not moving into
     * check cannot be checked here)
     */
    @Override
    HashSet<Integer> computeValidMoveSet() {
        HashSet<Integer> moveSet = new HashSet<Integer>();
        moveSet.add(this.position - 19);
        moveSet.add(this.position - 20);
        moveSet.add(this.position - 21);
        moveSet.add(this.position - 1);
        //moveSet.add(this.position - 2);
        moveSet.add(this.position + 1);
        //moveSet.add(this.position + 2);
        moveSet.add(this.position + 19);
        moveSet.add(this.position + 20);
        moveSet.add(this.position + 21);
        return moveSet;
    }
}